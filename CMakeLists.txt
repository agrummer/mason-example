cmake_minimum_required(VERSION 3.11)

project(AnalysisPayload)

find_package(ROOT)
find_package(AnalysisBase)

target_compile_definitions(ROOT::Core
  INTERFACE ${ROOT_DEFINITIONS}
)

add_executable(AnalysisPayload AnalysisPayload.cxx)

target_include_directories(AnalysisPayload
  PRIVATE
    ${AnalysisBase_INCLUDE_DIRS}
)

target_link_libraries(AnalysisPayload
  PRIVATE
    ROOT::Core ${ROOT_LIBRARIES}
    AnalysisBase::xAODEventInfo
    AnalysisBase::xAODRootAccess
    AnalysisBase::xAODJet
    AnalysisBase::xAODEgamma
    AnalysisBase::xAODMuon
    AnalysisBase::AsgTools
)
