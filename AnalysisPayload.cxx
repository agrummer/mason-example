// stdlib functionality
#include <fstream>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/EgammaContainer.h"
#include "xAODMuon/MuonContainer.h"
#include <AsgTools/MessageCheck.h>

namespace {

	struct Config {
		std::string input_file = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
		double min_pt = 50e3;
		double max_abs_eta = 2.5;
	};

	Config get_config(const std::string pathname) {
		std::ifstream filestream(pathname);
		if (filestream.bad()) {
			throw std::runtime_error("unable to parse config");
		}
		std::string line;
		Config config;
		while (std::getline(filestream, line)) {
			const auto split_pos = line.find(':');
			if (split_pos == std::string::npos) {
				throw std::runtime_error("unable to parse config");
			}
			const auto name = line.substr(0, split_pos);
			if (name == "input_file") {
				const auto value = line.substr(split_pos + 1);
				config.input_file = value;
			} else if (name == "min_pt") {
				const auto value = std::stod(line.substr(split_pos + 1));
				config.min_pt = value;
			} else if (name == "max_abs_eta") {
				const auto value = std::stod(line.substr(split_pos + 1));
				config.max_abs_eta = value;
			} else {
				throw std::runtime_error("unable to parse config");
			}
		}
		return config;
	}

}

using namespace asg::msgUserCode;

int main(const int argc, const char *const argv[]) {

  Config config;

  if (argc > 1) {
    config = get_config(argv[1]);
  }

  // initialize the xAOD EDM
  ANA_CHECK (xAOD::Init());

  // open the input file
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(config.input_file.c_str(), "READ") );
  ANA_CHECK (event.readFrom(iFile.get()));

  TString output_file_path = "/home/atlas/Bootcamp/build/plots.root";
  TFile out_file(output_file_path, "RECREATE");
  if (out_file.IsZombie()) {
    throw std::runtime_error("Unable to open output ROOT file " + output_file_path);
  }

  auto h_njets_raw = new TH1F("h_njets_raw", "Number of Jets;N Jets Per Event;N Events / Bin", 20, 0.0, 20.0);
  auto h_njets = new TH1F("h_njets", "Number of Jets;N Jets Per Event;N Events / Bin", 20, 0.0, 20.0);
  auto h_mjj_raw = new TH1F("h_mjj_raw", "Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events / Bin", 100, 0.0, 500.0);
  auto h_mjj = new TH1F("h_mjj", "Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events / Bin", 100, 0.0, 500.0);
  auto h_e_jet_delta_eta = new TH1F("h_e_jet_delta_eta", ";#Delta#eta;N Events / Bin", 100, 0.0, 6.0);
  auto h_e_jet_delta_phi = new TH1F("h_e_jet_delta_phi", ";#Delta#phi;N Events / Bin", 100, 0.0, 3.14159);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry(i);

    // Load xAOD::EventInfo and print the event info
    //const xAOD::EventInfo * ei = nullptr;
    //event.retrieve( ei, "EventInfo" );
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer *jets = nullptr;
    ANA_CHECK (event.retrieve(jets, "AntiKt4EMTopoJets"));

    const xAOD::EgammaContainer *electrons = nullptr;
    ANA_CHECK (event.retrieve(electrons, "Electrons"));

    const xAOD::MuonContainer *muons = nullptr;
    ANA_CHECK (event.retrieve(muons, "Muons"));

    //std::cout << "number of electrons: " << electrons->size() << std::endl;
    //std::cout << "number of muons: " << muons->size() << std::endl;

    h_njets_raw->Fill(jets->size());

    if (jets->size() >= 2) {
      h_mjj_raw->Fill(((*jets)[0]->p4() + (*jets)[1]->p4()).M() / 1000.0);
    }

    const xAOD::Jet *lead_jet = nullptr;
    const xAOD::Jet *sublead_jet = nullptr;

    unsigned njets = 0;

    // loop through all of the jets and make selections with the helper
    for(const auto *const jet : *jets) {
      if ( ! (jet->pt() >= config.min_pt && std::abs(jet->eta()) < config.max_abs_eta)) {
        continue;
      }
      ++njets;
      for (const auto *const electron : *electrons) {
        const auto delta_eta = std::abs(electron->eta() - jet->eta());
        const auto delta_phi = std::abs(electron->p4().DeltaPhi(jet->p4()));
        h_e_jet_delta_eta->Fill(delta_eta);
        h_e_jet_delta_phi->Fill(delta_phi);
      }
      //// print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      if (lead_jet == nullptr || jet->pt() > lead_jet->pt()) {
        if (lead_jet != nullptr) {
          sublead_jet = lead_jet;
        }
        lead_jet = jet;
      } else if (sublead_jet == nullptr || jet->pt() > sublead_jet->pt()) {
        sublead_jet = jet;
      }
    }

    h_njets->Fill(njets);

    if (lead_jet && sublead_jet) {
      h_mjj->Fill((lead_jet->p4() + sublead_jet->p4()).M() / 1000.0);
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  out_file.cd();
  h_njets_raw->Write();
  h_njets->Write();
  h_mjj_raw->Write();
  h_mjj->Write();
  h_e_jet_delta_eta->Write();
  h_e_jet_delta_phi->Write();

  // exit from the main function cleanly
  return 0;
}

